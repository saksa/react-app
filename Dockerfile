FROM nginx:1.13-alpine
RUN apk add --update tzdata && \
    cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && \
    apk del tzdata
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY build /usr/share/nginx/html
WORKDIR /usr/share/nginx/html